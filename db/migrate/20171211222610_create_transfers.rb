class CreateTransfers < ActiveRecord::Migration[5.1]
  def change
    create_table :transfers do |t|
      t.integer 'user_id'
      t.integer 'user_to_id'
      t.decimal 'amount', :default => 0, :precision => 2
      t.decimal 'commission', :default => 0, :precision => 2
      
      t.timestamps
    end
  end
end
