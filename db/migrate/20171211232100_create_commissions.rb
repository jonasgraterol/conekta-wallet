class CreateCommissions < ActiveRecord::Migration[5.1]
  def change
    create_table :commissions do |t|
      t.integer 'transfer_id'
      t.decimal 'transfer_amount', :default => 0, :precision => 2
      t.decimal 'percent', :default => 0, :precision => 2
      t.decimal 'commission_amount', :default => 0, :precision => 2
      t.decimal 'fixed_rate', :default => 0, :precision => 2
      t.timestamps
    end
  end
end
