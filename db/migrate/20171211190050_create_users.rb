class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.column 'name', :string
      t.string 'email', :default => '', :null => false
      t.string 'password', :limit => 20 
      t.decimal 'wallet_balance', :default => 0, :precision => 2

      t.timestamps
    end
  end
end
