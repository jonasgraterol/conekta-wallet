class FixDecimalsColumnsDefinitions < ActiveRecord::Migration[5.1]
  def change
    
    change_column('commissions', 'transfer_amount', :decimal, :precision => 10, :scale => 3)
    change_column('commissions', 'commission_percent', :decimal, :precision => 10, :scale => 3)
    change_column('commissions', 'commission_amount', :decimal, :precision => 10, :scale => 3)
    change_column('commissions', 'fixed_rate', :decimal, :precision => 10, :scale => 3)
    
    change_column('transfers', 'amount', :decimal, :precision => 10, :scale => 3)
    change_column('transfers', 'commission', :decimal, :precision => 10, :scale => 3)
    
    change_column('users', 'wallet_balance', :decimal, :precision => 10, :scale => 3)
    
  end
end
