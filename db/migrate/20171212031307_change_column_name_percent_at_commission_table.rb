class ChangeColumnNamePercentAtCommissionTable < ActiveRecord::Migration[5.1]
  def change
    rename_column('commissions', 'percent', 'commission_percent')
  end
end
