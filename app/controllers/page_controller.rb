class PageController < ApplicationController
  
  before_action :get_user_logged
  
  def index
  end

  def profile
    ##render layout: 'profile'
  end

  def register
  end

  def login
  end
  
  def get_user_logged
      if session[:user_id]
        @user = User.find(session[:user_id])
      else 
        @user = User.new
      end
  end
end
