class UsersController < ApplicationController
    
    def index
        @users = User.all
    end
    
    def show
    end
    
    def new
        @user = User.new
    end
    
    def create
        @user = User.new(user_params)
        
        respond_to do |format|
            if @user.save
                format.html {
                    flash[:notice] = "Bienvenido <strong>#{@user.name}</strong>, por favor inicia sesión"
                    redirect_to(:controller => 'page', :action => 'login')
                }
                format.json { render :show, status: :created, location: @user }
            else
                format.html {
                    flash[:error] = "Ocurrio un problema, intentelo de nuevo <br><small>#{@user.errors.full_messages}</small>"
                    redirect_to(:controller => 'page', :action => 'register')
                }
                format.json { render json: @user.errors, status: :unprocessable_entity }
            end
        end
    end
    
    def edit
    end
    
    def update
        respond_to do |format|
          if @user.update(user_params)
            format.html { redirect_to @user, notice: 'Usuario actualizado exitosamente.' }
            format.json { render :show, status: :ok, location: @user }
          else
            format.html { render :edit }
            format.json { render json: @user.errors, status: :unprocessable_entity }
          end
        end
    end
    
    def delete
    end
    
    def destroy
    end
    
    def addmoney
        @user = User.find(params[:addmoney][:user_id])
        @user.wallet_balance += BigDecimal(params[:addmoney][:amount])
        respond_to do |format|
            if @user.save
                 format.html {
                    flash[:success] = "Se agregaron <strong>$#{params[:addmoney][:amount]}</strong> exitosamente a tu cuenta."
                    redirect_to(:controller => 'page', :action => 'profile') 
                 }
                 format.json { render :show, status: :ok, location: @user }
            else
                format.html {
                    flash[:error] = "Ocurrio un problema por favor intentalo de nuevo."
                    redirect_to(:controller => 'page', :action => 'profile') 
                }
                format.json { render json: @user.errors, status: :unprocessable_entity }
            end
        end
    end
    
    def retiremoney
        @user = User.find(params[:retiremoney][:user_id])
        @user.wallet_balance -= BigDecimal(params[:retiremoney][:amount])
        respond_to do |format|
            if @user.save
                format.html {
                    flash[:success] = "Se retiraron <strong>$#{params[:retiremoney][:amount]}</strong> exitosamente de tu wallet a tu cuenta #{params[:retiremoney][:bank]}."
                    redirect_to(:controller => 'page', :action => 'profile') 
                }
                format.json { render :show, status: :ok, location: @user }
            else
                format.html {
                    flash[:error] = "Ocurrio un problema por favor intentalo de nuevo."
                    redirect_to(:controller => 'page', :action => 'profile') 
                }
                format.json { render :show, status: :ok, location: @user }
            end
        end
    end
    
    private
        def user_params
            params.require(:user).permit(:email, :name, :password)
        end
    
end
