class TransfersController < ApplicationController
    skip_before_action :verify_authenticity_token
    
    def index
    end
    
    def show
    end
    
    def new
    end
    
    def create
        @user_from = User.find(session[:user_id])
        @user_to = User.find(params[:transfer][:user_to_id])
        @transfer = Transfer.new(transfer_params)
        respond_to do |format|
            if @transfer.save
                
                @user_to.wallet_balance += @transfer.amount
                @user_to.save
                ##CALCULATE COMMISSION
                @commission = Commission.new(:transfer_amount => @transfer.amount)
            
                if @transfer.amount <= 1000
                    @commission.commission_percent = 3.0
                    @commission.fixed_rate = 8.00
                end
                if @transfer.amount > 1000 && @transfer.amount <= 5000
                    @commission.commission_percent = 2.5
                    @commission.fixed_rate = 6.00
                end
                if @transfer.amount > 5000 && @transfer.amount <= 10000
                    @commission.commission_percent = 2.0
                    @commission.fixed_rate = 4.00
                end
                if @transfer.amount > 10000
                    @commission.commission_percent = 1.0
                    @commission.fixed_rate = 3.00
                end
                
                @commission.commission_amount = @commission.transfer_amount*(@commission.commission_percent/100)
                ##END CALCULATE COMMISSION
                
                @transfer.commission = @commission
                
                @user_from.wallet_balance = @user_from.wallet_balance - @transfer.amount - @commission.commission_amount - @commission.fixed_rate
                @user_from.save
                format.html {
                    flash[:success] = "Transferencia exitosa a <strong>#{User.find(@transfer.user_to_id).name}</strong> por el monto de <strong>$#{@transfer.amount}</strong>"
                    flash[:notice] = "Se genero una comisión del <strong>#{@commission.commission_percent}% ($#{@commission.commission_amount})</strong> y una tasa fija de <strong>$#{@commission.fixed_rate}</strong>"
                    redirect_to(:controller => 'page', :action => 'profile')
                }
                format.json { render :show, status: :ok, location: @transfer }
            else
                format.html {
                    redirect_to('page/index')
                }
                format.json { render json: @transfer.errors, status: :unprocessable_entity }
            end
        end
    end
    
    def edit
    end
    
    def update
    end
    
    def delete
    end
    
    def destroy
    end
    
    private
    
        def transfer_params
            params.require(:transfer).permit(:user_id, :user_to_id, :amount)
        end
    
end
