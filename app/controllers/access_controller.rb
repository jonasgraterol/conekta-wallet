class AccessController < ApplicationController
    
    def attempt_login
        if params[:email].present? && params[:password].present?
           found_user = User.where(:email => params[:email]).first
           if found_user
              auth_user = found_user.authenticate(params[:password]) 
           end
        end
        
        respond_to do |format|
            if auth_user
                session[:user_id] = auth_user.id
                ##session[:user_name] = auth_user.name
                ##session[:user_email] = auth_user.email
                ##session[:user_balance] = auth_user.balance
                format.html {
                    flash[:notice] = "Bienvenido <strong>#{auth_user.name}</strong>."
                    redirect_to(:controller => 'page', :action => 'profile')
                }
                format.json { render :show, status: :ok, location: auth_user }
            else
                format.html {
                    flash[:error] = "Combinación de email/password invalida."
                    redirect_to(:controller => 'page', :action => 'login')
                }
                format.json { render json: {}, status: :unprocessable_entity }
            end
        end
        
        
    end
    
    def logout
        respond_to do |format|
            format.html {
                session[:user_id] = nil
                flash[:notice] = "Haz cerrado sesión."
                redirect_to(:controller => 'page', :action => 'index')
            }
            format.json { render :show, status: :ok }
        end
    end
end
