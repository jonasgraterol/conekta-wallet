class Transfer < ApplicationRecord
    belongs_to :user
    has_one :commission
    
    validates_presence_of :user_id, :user_to_id, :amount
    validates_numericality_of :amount
end
