class Commission < ApplicationRecord
    belongs_to :transfer
    
    validates_presence_of :transfer_id, :transfer_amount
    validates_numericality_of :transfer_amount, :commission_percent, :commission_amount, :fixed_rate
end
