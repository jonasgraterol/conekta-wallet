Rails.application.routes.draw do
  
  root 'page#index'
  
  post 'access/attempt_login'
  get 'access/logout'
  
  resources :users do
    member do
      get :delete
    end
  end
  post 'users/addmoney'
  post 'users/retiremoney'
  
  resources :transfers do
    member do
      get :delete
    end
  end
  
  resources :commissions do
    member do
      get :delete
    end
  end
  
  get 'page/index'

  get 'page/profile'

  get 'page/register'

  get 'page/login'
  
  

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
