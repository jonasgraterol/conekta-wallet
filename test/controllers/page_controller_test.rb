require 'test_helper'

class PageControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get page_index_url
    assert_response :success
  end

  test "should get profile" do
    get page_profile_url
    assert_response :success
  end

  test "should get register" do
    get page_register_url
    assert_response :success
  end

  test "should get login" do
    get page_login_url
    assert_response :success
  end

end
